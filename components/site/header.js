import React from 'react';

const Header = () => {
    return (
        <div> 
             <header>
        <div className="header-area ">
          <div className="header-top_area d-none d-lg-block">
            <div className="container">
              <div className="row">
                <div className="col-xl-5 col-md-5 ">
                  <div className="header_left">
                    <p>Chào mừng bạn đến bất động sản hạnh phúc</p>
                  </div>
                </div>
                <div className="col-xl-7 col-md-7">
                  <div className="header_right d-flex">
                    <div className="short_contact_list">
                      <ul>
                        <li><a href="#"> <i className="fa fa-envelope" /> nodejs@docmed.com</a></li>
                      </ul>
                    </div>
                    <div className="social_media_links">
                      <a href="#">
                        <i className="fa fa-linkedin" />
                      </a>
                      <a href="#">
                        <i className="fa fa-facebook" />
                      </a>
                      <a href="#">
                        <i className="fa fa-google-plus" />
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="sticky-header" className="main-header-area">
            <div className="container">
              <div className="header_bottom_border">
                <div className="row align-items-center">
                  <div className="col-xl-3 col-lg-2">
                    <div className="logo">
                      <a href="index.html">
                        <img src="img/logo.png" alt="" />
                      </a>
                    </div>
                  </div>
                  <div className="col-xl-6 col-lg-7">
                    <div className="main-menu  d-none d-lg-block">
                      <nav>
                        <ul id="navigation">
                          <li><a className="active" href="index.html">Trang chủ</a></li>
                          <li><a href="#">Bất động sản</a></li>
                          <li><a href="blog.html">Tin tức</a></li>
                          <li><a href="contact.html">Liên hệ</a></li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 d-none d-lg-block">
                    <div className="Appointment">
                      <div className="book_btn d-none d-lg-block">
                        <a href="#"> <i className="fa fa-phone" /> 1234567890</a>
                      </div>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="mobile_menu d-block d-lg-none" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
        </div>
    )
}
export default Header;