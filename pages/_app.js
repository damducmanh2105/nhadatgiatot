import Header from "../components/site/header";
import Footer from "../components/site/footer";
import './../styles/animate.css'
import './../styles/bootstrap.min.css'
// import './../styles/flaticon.css'
// import './../styles/font-awesome.min.css'
// import './../styles/gijgo.css'
import './../styles/globals.css'
import './../styles/magnific-popup.css'
import './../styles/nice-select.css'
// import './../styles/owl.carousel.min.css'
import './../styles/slick.css'
import './../styles/slicknav.css'
// import './../styles/theme-default.css'
// import './../styles/themify-icons.css'
import './../styles/style.css'
const WrappedApp = (props) => {
  const { Component, pageProps } = props;
  return (
      <>
          <Header />
          <Component {...pageProps} />
          <Footer />
      </>
  );
};

export default WrappedApp
